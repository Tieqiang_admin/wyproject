package com.oaker.hours.doman.vo;


import com.baomidou.mybatisplus.annotations.TableField;
import com.oaker.hours.doman.entity.MhArchiveHour;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "MhArchiveDetailVO", description = "归档查询详情类")
public class MhArchiveDetailVO {


    @ApiModelProperty("id")
    private Long Id;

    @ApiModelProperty("部门ID")
    private Long deptId;

    @ApiModelProperty("月份")
    private String archiveDate;

//    @ApiModelProperty("填报状态")
//    private boolean status;

    @ApiModelProperty("归档工时")
    private List<MhArchiveHour> hourList = new ArrayList<>();

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    private Date createTime;

    /** 创建人 */
    @ApiModelProperty("创建人")
    private int createUser;
}
